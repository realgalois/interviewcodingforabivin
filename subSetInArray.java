import java.util.Arrays;
//import java.util.*;
import java.util.HashSet;
import java.util.Set;
/** to find pairs of number with sum equaling to a given Integer 

* @author dtbinh
* This solution use brute force algorithm and it's time complexity is very hight, O(n^2)
*/
public class subSetInArray{
	public static void main(String args[]){
		int[] listNumbers ={2,4,3,5,7,9,8};
		int[] secondlistNumbers ={2,4,3,5,6,-2,4,7,8};
		printNumbers(listNumbers,7);
		printNumbers(secondlistNumbers,7);
		randomPrint(makeRandomArray(9), 11);
        randomPrint(makeRandomArray(10), 12);
        upgradeSolutionPrint(makeRandomArray(10),8);
        hashSetSolutionPrint(makeRandomArray(10),8);
	}
	
	public static void findPairs(int[] array, int sum){
		for(int i=0; i<array.length; i++){
			int first =array[i];
			for(int j=i+1; j<array.length; j++){
				int second =array[j];
				if(first+second ==sum){
					//System.out.printf("(%d,%d) %n",first, second);
					System.out.println("(%d,%d) %n",first, second);
				}
			}
		}
	}
	
	public static void findPairsAterSorting(int[] numbers, int k){ 
		if(numbers.length < 2){ 
			return; 
		} 
		Arrays.sort(numbers); 
		int left = 0; int right = numbers.length -1; 
		while(left < right){ 
			int sum = numbers[left] + numbers[right]; 
			if(sum == k){ 
				System.out.printf("(%d, %d) %n", numbers[left], numbers[right]); 
				left = left + 1; 
				right = right -1; 
				}
				else if(sum < k){ 
							left = left +1; 
						}
						else if (sum > k) { 
							right = right -1; 
						} 
		} 
	}
	
	



	public static void findPairsUsingSet(int[] numbers, int n){ 
		if(numbers.length < 2){ 
			return; 
		} 
		Set<Integer> set = new HashSet<Integer>(numbers.length);
		for(int value : numbers){ 
			int search = n - value; 
			// if target number is not in set then add 
			if(!set.contains(search)){ 
			set.add(value); 
			}
			else { 
					System.out.printf("(%d, %d) %n", value, search); 
			} 
		} 
	}


	public static int[] makeRandomArray(int length){ 
		int[] randoms = new int[length]; 
		for(int i=0; i<length; i++){ 
		   randoms[i] = (int) (Math.random()*15);
		    } 
			return randoms; 
		} 




	public static void printNumbers(int[] givenArray, int givenSum){ 
		System.out.println("fixed array : " + Arrays.toString(givenArray)); 
		System.out.println("Given sum : " + givenSum); 
		System.out.println("Integer numbers, whose sum is equal to value : " + givenSum);
		findPairs(givenArray, givenSum); 
		
		}

	
	public static void randomPrint(int[] random, int k){ 
		System.out.println("Random Integer array : " + Arrays.toString(random));
		System.out.println("Sum : " + k); 
		System.out.println("pair of numbers from an array whose sum equals " + k); 
		findPairs(random, k); 
	 	}
	public static void hashSetSolutionPrint(int[] random, int k){ 
		System.out.println("//////////////////////////////////////// " );
		System.out.println("//////////////////////////////////////// " );

		System.out.println("SOLUTION WITH HARSHSET:" );
		System.out.println("Random Integer array : " + Arrays.toString(random));
		System.out.println("Sum : " + k); 
		System.out.println("pair of numbers from an array whose sum equals " + k); 
		findPairsUsingSet(random, k); 
	 	}

	public static void upgradeSolutionPrint(int[] random, int k){ 
		System.out.println("//////////////////////////////////////// " );
		System.out.println("//////////////////////////////////////// " );

		System.out.println("THIS IS UPGRDED VERSION FOR THE SOLUTION:" );
		System.out.println("Random Integer array : " + Arrays.toString(random));
		System.out.println("Sum : " + k); 
		System.out.println("pair of numbers from an array whose sum equals " + k); 
		findPairsAterSorting(random, k); 

	 	}

	


}