import java.util.*;
 
class reversedString
{
   public static void main(String args[])
   {
      String enteredString, resultSting ="";
      Scanner in = new Scanner(System.in);
      System.out.println("Enter a string to reverse");
      
      enteredString = in.nextLine();
 
      int length = enteredString.length();
 
      for ( int i = length - 1 ; i >= 0 ; i-- )
         resultSting = resultSting + enteredString.charAt(i);
 
      System.out.println("The reversed String is: "+resultSting);
   }
}  